package br.com.joyjet.service;

import static br.com.joyjet.util.LoadFileUtil.readJsonBy;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.joyjet.domain.CartVO;
import br.com.joyjet.domain.RequestVO;
import br.com.joyjet.domain.ResponseVO;

public class VirtualStoreServiceTest {
	
	private VirtualStoreService service = new VirtualStoreService();
	private ObjectMapper mapper = new ObjectMapper();
	
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();
	
	@Test
	public void shouldGroupByCardsByTotalPrice() throws Exception {
		RequestVO requestVO = mapper.readValue(readJsonBy("data.json"), RequestVO.class);
		ResponseVO responseCartsVO = mapper.readValue(readJsonBy("output.json"), ResponseVO.class);
		
		List<CartVO> cartsVO = service.calculateTotalPriceForCartsWithFees(requestVO);
		assertThat(cartsVO.size(), equalTo(3));
		assertThat(cartsVO.size(), equalTo(responseCartsVO.getCarts().size()));
		assertThat(responseCartsVO.getCarts().size(), equalTo(3));
		assertThat(cartsVO, equalTo(responseCartsVO.getCarts()));
	}
	
	@Test
	public void shouldThrowIllegalArgumentExceptionWhenRequestVOisNull() {
		expectedEx.expect(IllegalArgumentException.class);
	    expectedEx.expectMessage("Request is not null.");
	    
		service.calculateTotalPriceForCartsWithFees(null);
	}
	
	@Test
	public void shouldThrowIllegalArgumentExceptionWhenCartsisNull() {
		expectedEx.expect(IllegalArgumentException.class);
	    expectedEx.expectMessage("Carts is not null.");
	    
		service.calculateTotalPriceForCartsWithFees(new RequestVO());
	}
	
	@Test
	public void shouldThrowIllegalArgumentExceptionWhenArticlesisNull() {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("Articles is not null.");
		
		RequestVO requestVO = new RequestVO();
		requestVO.setCarts(new ArrayList<>());
		service.calculateTotalPriceForCartsWithFees(requestVO);
	}
	
	@Test
	public void shouldThrowIllegalArgumentExceptionWhenDeliveryFeesisNull() {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("Delivery Fees is not null.");
		
		RequestVO requestVO = new RequestVO();
		requestVO.setCarts(new ArrayList<>());
		requestVO.setArticles(new ArrayList<>());
		service.calculateTotalPriceForCartsWithFees(requestVO);
	}
	
	@Test
	public void shouldGroupByCardsByTotalPriceWithEmptyArticlesAndCarts() {
		RequestVO requestVO = new RequestVO();
		requestVO.setCarts(new ArrayList<>());
		requestVO.setArticles(new ArrayList<>());
		requestVO.setDeliveryFees(new ArrayList<>());
		List<CartVO> cartsVO = service.calculateTotalPriceForCartsWithFees(requestVO);
		assertThat(cartsVO.size(), equalTo(0));
	}
}
