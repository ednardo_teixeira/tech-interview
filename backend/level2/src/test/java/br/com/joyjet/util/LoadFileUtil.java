package br.com.joyjet.util;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.commons.io.IOUtils;

public final class LoadFileUtil {
	
	public static String readJsonBy(String name) throws IOException, URISyntaxException {
		return IOUtils.toString(LoadFileUtil.class.getClassLoader().getResource(name).toURI());
	}
}
