package br.com.joyjet.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.joyjet.domain.Cart;
import br.com.joyjet.domain.CartVO;
import br.com.joyjet.domain.DeliveryFee;
import br.com.joyjet.domain.EligibleTransactionVolume;
import br.com.joyjet.domain.RequestVO;

@Service(value="virtualStoreService")
public class VirtualStoreService implements IVirtualStoreService {

	/**
	 * Method responsible in recovery Discovery Fees by total price cart
	 * @param total
	 * @param deliveryFees
	 * @return
	 */
	private BigDecimal recoveryDeliveryFee(BigDecimal total, List<DeliveryFee> deliveryFees) {
		EligibleTransactionVolume eligibleTransactionVolume;
		Double price = 0d;
		for (DeliveryFee deliveryFee : deliveryFees) {
			eligibleTransactionVolume = deliveryFee.getEligibleTransactionVolume();
			if (eligibleTransactionVolume != null) {
				if (eligibleTransactionVolume.getMaxPrice() == null && total.doubleValue() >= eligibleTransactionVolume.getMinPrice()) {
					price += deliveryFee.getPrice();
					break;
				} else if (total.doubleValue() >= eligibleTransactionVolume.getMinPrice() && 
						total.doubleValue() <= (eligibleTransactionVolume.getMaxPrice() - 1d)) {
					price += deliveryFee.getPrice();
					break;
				}
			}
		}
		
		return BigDecimal.valueOf(price);
	}

	/**
	 * Method Calculate All carts by total price add Discovery Fees
	 */
	@Override
	public List<CartVO> calculateTotalPriceForCartsWithFees(RequestVO requestVO) {
		Assert.notNull(requestVO, "Request is not null.");
		Assert.notNull(requestVO.getCarts(), "Carts is not null.");
		Assert.notNull(requestVO.getArticles(), "Articles is not null.");
		Assert.notNull(requestVO.getDeliveryFees(), "Delivery Fees is not null.");
		
		List<CartVO> cartsResult = new ArrayList<>();
		
		//Carts RequestVO
		List<Cart> carts = requestVO.getCarts();
		
		CartVO cartVO;
		BigDecimal total;
		for (Cart cart : carts) {
			total = cart.calculateTotalItems();
			total = total.add(this.recoveryDeliveryFee(total, requestVO.getDeliveryFees()));
			cartVO = new CartVO(cart.getId(), total.setScale(0, RoundingMode.DOWN).doubleValue()); //Around Down value
			cartsResult.add(cartVO);
		}
		
		return cartsResult;
	}
}
