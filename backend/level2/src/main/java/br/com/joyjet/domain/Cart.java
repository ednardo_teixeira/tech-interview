package br.com.joyjet.domain;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.util.Assert;

public class Cart extends Entity {

	private static final long serialVersionUID = 1L;

	private List<ItemCart> items;

	public List<ItemCart> getItems() {
		return items;
	}

	public void setItems(List<ItemCart> items) {
		this.items = items;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((getItems() == null) ? 0 : getItems().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Cart)) {
			return false;
		}
		Cart other = (Cart) obj;
		if (getItems() == null) {
			if (other.getItems() != null) {
				return false;
			}
		} else if (!getItems().equals(other.getItems())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Cart [getItems()=" + getItems() + ", getId()=" + getId() + "]";
	}

	/**
	 * Calculate total price by cart
	 * @param mapArticleDiscount
	 * @return
	 */
	public BigDecimal calculateTotalItems() {
		Assert.notNull(items, "Items Carts is not null.");
		
		BigDecimal total = BigDecimal.ZERO;
		
		//Loop in items cart to calculate total
		for (ItemCart itemCart : items) {
			if (itemCart.getArticleEntity() == null) {
				continue;
			}
			
 			total = total.add(BigDecimal.valueOf(itemCart.getQuantity())
 					.multiply(BigDecimal.valueOf(itemCart.getArticleEntity().getPrice())));
		}
		
		return total;
	}
}
