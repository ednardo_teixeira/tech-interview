package br.com.joyjet.domain;

import java.io.Serializable;
import java.util.List;

public class ResponseVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private List<CartVO> carts;

	public List<CartVO> getCarts() {
		return carts;
	}

	public void setCarts(List<CartVO> carts) {
		this.carts = carts;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((carts == null) ? 0 : carts.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponseVO other = (ResponseVO) obj;
		if (carts == null) {
			if (other.carts != null)
				return false;
		} else if (!carts.equals(other.carts))
			return false;
		return true;
	}
}
