package br.com.joyjet.context;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"br.com.joyjet.service", "br.com.joyjet.controller"})
@EnableAutoConfiguration
public class TestJoyjetApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestJoyjetApplication.class, args);
	}
}
