package br.com.joyjet.service;

import java.util.List;

import br.com.joyjet.domain.CartVO;
import br.com.joyjet.domain.RequestVO;

public interface IVirtualStoreService {
	
	public List<CartVO> calculateTotalPriceForCartsWithFees(RequestVO requestVO);
}
