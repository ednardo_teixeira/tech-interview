package br.com.joyjet.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EligibleTransactionVolume implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty(value="min_price")
	private double minPrice;
	
	@JsonProperty(value="max_price")
	private Double maxPrice;

	public double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(double minPrice) {
		this.minPrice = minPrice;
	}

	public Double getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(Double maxPrice) {
		this.maxPrice = maxPrice;
	}

	@Override
	public String toString() {
		return "EligibleTransactionVolume [getMinPrice()=" + getMinPrice() + ", getMaxPrice()=" + getMaxPrice() + "]";
	}
}
