package br.com.joyjet.domain;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Article> articles;
	private List<Cart> carts;
	@JsonProperty(value="delivery_fees")
	private List<DeliveryFee> deliveryFees;

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public List<Cart> getCarts() {
		return carts;
	}

	public void setCarts(List<Cart> carts) {
		this.carts = carts;
	}

	public List<DeliveryFee> getDeliveryFees() {
		return deliveryFees;
	}

	public void setDeliveryFees(List<DeliveryFee> deliveryFees) {
		this.deliveryFees = deliveryFees;
	}
}
