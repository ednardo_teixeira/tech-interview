package br.com.joyjet.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.joyjet.domain.Cart;
import br.com.joyjet.domain.CartVO;
import br.com.joyjet.domain.RequestVO;

@Service(value="virtualStoreService")
public class VirtualStoreService implements IVirtualStoreService {

	/**
	 * Method Calculate All carts by total price
	 */
	@Override
	public List<CartVO> calculateTotalPriceForCarts(RequestVO requestVO) {
		Assert.notNull(requestVO, "Request is not null.");
		Assert.notNull(requestVO.getCarts(), "Carts is not null.");
		Assert.notNull(requestVO.getArticles(), "Articles is not null.");
		
		List<CartVO> cartsResult = new ArrayList<>();
		
		//Carts RequestVO
		List<Cart> carts = requestVO.getCarts();
		
		CartVO cartVO;
		BigDecimal total;
		for (Cart cart : carts) {
			//Calculate Total Without Discount
			total = cart.calculateTotalItems();
			cartVO = new CartVO(cart.getId(), total.setScale(0, RoundingMode.DOWN).doubleValue());
			cartsResult.add(cartVO);
		}
		
		return cartsResult;
	}
}
