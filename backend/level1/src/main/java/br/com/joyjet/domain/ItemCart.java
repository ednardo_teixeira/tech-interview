package br.com.joyjet.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

public class ItemCart implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
	@JsonIdentityReference(alwaysAsId=true)
	@JsonProperty(value="article_id")
	private Article articleEntity;
	private Double quantity;

	public Article getArticleEntity() {
		return articleEntity;
	}

	public void setArticleEntity(Article articleEntity) {
		this.articleEntity = articleEntity;
	}

	public Double getQuantity() {
		return quantity == null? 0d: quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((getArticleEntity() == null) ? 0 : getArticleEntity().hashCode());
		result = prime * result + ((getQuantity() == null) ? 0 : getQuantity().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof ItemCart)) {
			return false;
		}
		ItemCart other = (ItemCart) obj;
		if (getArticleEntity() == null) {
			if (other.getArticleEntity() != null) {
				return false;
			}
		} else if (!getArticleEntity().equals(other.getArticleEntity())) {
			return false;
		}
		if (getQuantity() == null) {
			if (other.getQuantity() != null) {
				return false;
			}
		} else if (!getQuantity().equals(other.getQuantity())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ItemCart [getArticleEntity()=" + getArticleEntity() + ", getQuantity()=" + getQuantity() + "]";
	}
}
