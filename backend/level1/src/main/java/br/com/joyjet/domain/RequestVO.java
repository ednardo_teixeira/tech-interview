package br.com.joyjet.domain;

import java.io.Serializable;
import java.util.List;

public class RequestVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Article> articles;
	private List<Cart> carts;

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public List<Cart> getCarts() {
		return carts;
	}

	public void setCarts(List<Cart> carts) {
		this.carts = carts;
	}
}
