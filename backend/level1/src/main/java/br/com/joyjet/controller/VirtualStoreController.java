package br.com.joyjet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.joyjet.domain.RequestVO;
import br.com.joyjet.domain.ResponseVO;
import br.com.joyjet.service.VirtualStoreService;

@Controller
@RequestMapping(value="/virtual-store")
public class VirtualStoreController {

	@Autowired
	private VirtualStoreService virtualStoreService;
	
	@RequestMapping(value="/calc-carts")
	public @ResponseBody ResponseVO calculateTotalPriceForCarts(@RequestBody RequestVO requestVO) {
		ResponseVO responseVO = new ResponseVO();
		responseVO.setCarts(this.virtualStoreService.calculateTotalPriceForCarts(requestVO));
		return responseVO;
	}
}
