package br.com.joyjet.domain;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.joyjet.util.LoadFileUtil;

public class CartTest {

	@Rule
	public ExpectedException expectedEx = ExpectedException.none();
	
	private ObjectMapper mapper = new ObjectMapper();
	
	@Test
	public void shouldThrowIllegalArgumentExceptionWhenItemsCartsisNull() {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("Items Carts is not null.");
		
		new Cart().calculateTotalItems();
	}

	@Test
	public void shouldCalculateTotalCart() throws IOException, URISyntaxException {
		Cart cart = mapper.readValue(LoadFileUtil.readJsonBy("cart.json"), Cart.class);
		
		BigDecimal result = cart.calculateTotalItems();
		assertThat(result, equalTo(new BigDecimal(600d).setScale(2)));
	}
}
