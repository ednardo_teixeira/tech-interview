package br.com.joyjet.controller;

import static br.com.joyjet.util.LoadFileUtil.readJsonBy;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.joyjet.context.TestJoyjetApplication;
import br.com.joyjet.domain.ResponseVO;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = TestJoyjetApplication.class)
public class VirtualStoreControllerTest {

	private final String BASE_URL = "/virtual-store";

	private MockMvc mockMvc;
	
	@Autowired
	private VirtualStoreController virtualStoreController;
	
	private ObjectMapper mapper = new ObjectMapper();
	
	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(virtualStoreController).build();
	}

	@Test
	public void shouldRequestForTotalCarts() throws Exception {
		virtualStoreController = mock(VirtualStoreController.class);
		
		String jsonReponse = mockMvc.perform(post(BASE_URL.concat("/calc-carts"))
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)
				.content(readJsonBy("data.json")))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();
		
		String jsonExpected = readJsonBy("output.json");
		
		ResponseVO responseVO = mapper.readValue(jsonReponse, ResponseVO.class);
		ResponseVO expectedResponseVO = mapper.readValue(jsonExpected, ResponseVO.class);
		
		assertThat(responseVO, equalTo(expectedResponseVO));
	}
}
