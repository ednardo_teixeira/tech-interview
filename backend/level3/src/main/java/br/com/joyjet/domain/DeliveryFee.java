package br.com.joyjet.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeliveryFee implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonProperty(value="eligible_transaction_volume")
	private EligibleTransactionVolume eligibleTransactionVolume;
	private Double price;

	public EligibleTransactionVolume getEligibleTransactionVolume() {
		return eligibleTransactionVolume;
	}

	public void setEligibleTransactionVolume(EligibleTransactionVolume eligibleTransactionVolume) {
		this.eligibleTransactionVolume = eligibleTransactionVolume;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "DeliveryFee [getEligibleTransactionVolume()=" + getEligibleTransactionVolume() + ", getPrice()="
				+ getPrice() + "]";
	}
}
