package br.com.joyjet.domain.discount;

import java.math.BigDecimal;

import br.com.joyjet.domain.AbstractDiscount;

public class AmountDiscount extends AbstractDiscount {

	private static final long serialVersionUID = 1L;

	@Override
	public BigDecimal recoveryDiscountBy(BigDecimal valueCart) {
		if (valueCart == null) {
			return BigDecimal.ZERO;
		}
		
		return BigDecimal.valueOf(this.getValue());
	}
}
