package br.com.joyjet.domain.discount;

import java.math.BigDecimal;

import br.com.joyjet.domain.AbstractDiscount;

public class PercentageDiscount extends AbstractDiscount {

	private static final long serialVersionUID = 1L;

	@Override
	public BigDecimal recoveryDiscountBy(BigDecimal valueCart) {
		if (valueCart == null) {
			return BigDecimal.ONE;
		}
	
		return valueCart.multiply(BigDecimal.valueOf(this.getValue())).divide(BigDecimal.valueOf(100d));
	}
}
