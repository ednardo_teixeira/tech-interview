package br.com.joyjet.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.joyjet.domain.AbstractDiscount;
import br.com.joyjet.domain.Cart;
import br.com.joyjet.domain.CartVO;
import br.com.joyjet.domain.DeliveryFee;
import br.com.joyjet.domain.EligibleTransactionVolume;
import br.com.joyjet.domain.RequestVO;

@Service(value="virtualStoreService")
public class VirtualStoreService implements IVirtualStoreService {

	@Override
	public List<CartVO> calculateTotalPriceForCartsWithDiscountAndFees(RequestVO requestVO) {
		Assert.notNull(requestVO, "Request is not null.");
		Assert.notNull(requestVO.getCarts(), "Carts is not null.");
		Assert.notNull(requestVO.getArticles(), "Articles is not null.");
		Assert.notNull(requestVO.getDeliveryFees(), "Delivery Fees is not null.");
		Assert.notNull(requestVO.getDiscounts(), "Discounts is not null.");
		
		List<CartVO> cartsResult = new ArrayList<>();
		
		//Carts RequestVO
		List<Cart> carts = requestVO.getCarts();
		Map<Integer, List<AbstractDiscount>> mapArticleDiscount = this.mountMapArticleByDiscount(requestVO);
		
		CartVO cartVO;
		BigDecimal total;
		for (Cart cart : carts) {
			total = cart.calculateTotalItems(mapArticleDiscount);
			total = total.add(this.recoveryPriceDeliveryFee(total, requestVO.getDeliveryFees()));
			cartVO = new CartVO(cart.getId(), total.setScale(0, RoundingMode.DOWN).doubleValue());
			cartsResult.add(cartVO);
		}
		
		return cartsResult;
	}
	
	/**
	 * Recovery Price Delivery Fee by total cart 
	 * @param total
	 * @param deliveryFees
	 * @return
	 */
	private BigDecimal recoveryPriceDeliveryFee(BigDecimal total, List<DeliveryFee> deliveryFees) {
		EligibleTransactionVolume eligibleTransactionVolume;
		Double price = 0d;
		for (DeliveryFee deliveryFee : deliveryFees) {
			eligibleTransactionVolume = deliveryFee.getEligibleTransactionVolume();
			if (eligibleTransactionVolume != null) {
				if (eligibleTransactionVolume.getMaxPrice() == null && total.doubleValue() >= eligibleTransactionVolume.getMinPrice()) {
					price += deliveryFee.getPrice();
					break;
				} else if (total.doubleValue() >= eligibleTransactionVolume.getMinPrice() && 
						total.doubleValue() <= (eligibleTransactionVolume.getMaxPrice() - 1d)) {
					price += deliveryFee.getPrice();
					break;
				}
			}
		}
		
		return BigDecimal.valueOf(price);
	}

	/**
	 * Mount Map to article by discount used in calculate cart total
	 * @param requestVO
	 * @return
	 */
	private Map<Integer, List<AbstractDiscount>> mountMapArticleByDiscount(RequestVO requestVO) {
		Map<Integer, List<AbstractDiscount>> map = new HashMap<>();
		List<AbstractDiscount> discounts = requestVO.getDiscounts();
		
		for (AbstractDiscount abstractDiscount : discounts) {
			if (abstractDiscount.getArticle() != null) {
				if (!map.containsKey(abstractDiscount.getArticle().getId())) {
					map.put(abstractDiscount.getArticle().getId(), new ArrayList<>());
				}
				
				map.get(abstractDiscount.getArticle().getId()).add(abstractDiscount);
			}
		}
		
		return map;
	}
}
