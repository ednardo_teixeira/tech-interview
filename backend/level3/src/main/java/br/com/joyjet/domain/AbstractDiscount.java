package br.com.joyjet.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.joyjet.domain.discount.AmountDiscount;
import br.com.joyjet.domain.discount.PercentageDiscount;

@JsonTypeInfo(
		use = Id.NAME, 
		include = JsonTypeInfo.As.PROPERTY, 
		property = "type",
		visible=true)
@JsonSubTypes({ 
	@Type(value = PercentageDiscount.class, name = "percentage"),
	@Type(value = AmountDiscount.class, name = "amount")})
public abstract class AbstractDiscount implements Serializable {

	private static final long serialVersionUID = 1L;

	@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
	@JsonIdentityReference(alwaysAsId=true)
	@JsonProperty(value="article_id")
	private Article article;
	private String type;
	private double value;

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "AbstractDiscount [getArticle()=" + getArticle() + ", getType()=" + getType() + ", getValue()="
				+ getValue() + "]";
	}
	
	/**
	 * Abstract Method to calculate discount discriminator
	 * @param valueCart
	 * @return
	 */
	public abstract BigDecimal recoveryDiscountBy(BigDecimal valueCart);
}
