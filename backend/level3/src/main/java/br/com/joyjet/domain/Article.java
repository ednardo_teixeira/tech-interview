package br.com.joyjet.domain;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Article extends Entity {

	private static final long serialVersionUID = 1L;

	private String name;
	private Double price;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price == null? 0d: price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		result = prime * result + ((getPrice() == null) ? 0 : getPrice().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Article)) {
			return false;
		}
		Article other = (Article) obj;
		if (getName() == null) {
			if (other.getName() != null) {
				return false;
			}
		} else if (!getName().equals(other.getName())) {
			return false;
		}
		if (getPrice() == null) {
			if (other.getPrice() != null) {
				return false;
			}
		} else if (!getPrice().equals(other.getPrice())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Article [getName()=" + getName() + ", getPrice()=" + getPrice() + ", getId()=" + getId() + "]";
	}

	/**
	 * Get price article with discount.
	 * @param mapArticleDiscount
	 * @return
	 */
	@JsonIgnore
	public BigDecimal getPriceDiscount(Map<Integer, List<AbstractDiscount>> mapArticleDiscount) {
		BigDecimal priceDiscount = BigDecimal.valueOf(getPrice());
		if (mapArticleDiscount.containsKey(getId())) {
			List<AbstractDiscount> discounts = mapArticleDiscount.get(getId());
			for (AbstractDiscount abstractDiscount : discounts) {
				priceDiscount = priceDiscount.subtract(abstractDiscount.recoveryDiscountBy(priceDiscount));
			}
		}
		
		return priceDiscount;
	}
}
