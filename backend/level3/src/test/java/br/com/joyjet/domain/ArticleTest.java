package br.com.joyjet.domain;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import br.com.joyjet.domain.discount.AmountDiscount;
import br.com.joyjet.domain.discount.PercentageDiscount;

public class ArticleTest {

	@Test
	public void shouldCalculatePercentageDiscountByArticle() {
		Article article = buildArticle();
		
		Map<Integer, List<AbstractDiscount>> discountsByArticle = new HashMap<>();
		PercentageDiscount percentageDiscount = new PercentageDiscount();
		percentageDiscount.setValue(10);
		discountsByArticle.put(article.getId(), Arrays.asList(percentageDiscount));
		
		BigDecimal result = article.getPriceDiscount(discountsByArticle);
		assertThat(result, equalTo(new BigDecimal(900d).setScale(1)));
	}
	
	@Test
	public void shouldCalculatePercentageAndAmountDiscountByArticle() {
		Article article = buildArticle();
		
		Map<Integer, List<AbstractDiscount>> discountsByArticle = new HashMap<>();
		PercentageDiscount percentageDiscount = new PercentageDiscount();
		percentageDiscount.setValue(10);

		AmountDiscount amountDiscount = new AmountDiscount();
		amountDiscount.setValue(40);
		
		discountsByArticle.put(article.getId(), Arrays.asList(percentageDiscount, amountDiscount));
		
		BigDecimal result = article.getPriceDiscount(discountsByArticle);
		assertThat(result, equalTo(new BigDecimal(860d).setScale(1)));
	}

	@Test
	public void shouldCalculateAmountDiscountByArticle() {
		Article article = buildArticle();
		
		Map<Integer, List<AbstractDiscount>> discountsByArticle = new HashMap<>();
		AmountDiscount amountDiscount = new AmountDiscount();
		amountDiscount.setValue(40);
		
		discountsByArticle.put(article.getId(), Arrays.asList(amountDiscount));
		
		BigDecimal result = article.getPriceDiscount(discountsByArticle);
		assertThat(result, equalTo(new BigDecimal(960d).setScale(1)));
	}

	private Article buildArticle() {
		Article article = new Article();
		article.setId(1);
		article.setName("Product A");
		article.setPrice(1000d);
		
		return article;
	}
}
