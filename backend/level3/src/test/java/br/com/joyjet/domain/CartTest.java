package br.com.joyjet.domain;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.joyjet.domain.discount.AmountDiscount;
import br.com.joyjet.util.LoadFileUtil;

public class CartTest {

	@Rule
	public ExpectedException expectedEx = ExpectedException.none();
	
	private ObjectMapper mapper = new ObjectMapper();
	
	@Test
	public void shouldThrowIllegalArgumentExceptionWhenDiscountsisNull() {
		expectedEx.expect(IllegalArgumentException.class);
	    expectedEx.expectMessage("Discounts is not null.");
	    
		new Cart().calculateTotalItems(null);
	}
	
	@Test
	public void shouldThrowIllegalArgumentExceptionWhenItemsCartsisNull() {
		expectedEx.expect(IllegalArgumentException.class);
		expectedEx.expectMessage("Items Carts is not null.");
		
		new Cart().calculateTotalItems(new HashMap<>());
	}

	@Test
	public void shouldCalculateTotalCartByAmountDiscount() throws IOException, URISyntaxException {
		Cart cart = mapper.readValue(LoadFileUtil.readJsonBy("cart.json"), Cart.class);
		Map<Integer, List<AbstractDiscount>> discountsByArticle = new HashMap<>();
		AmountDiscount amountDiscount = new AmountDiscount();
		amountDiscount.setValue(50);
		discountsByArticle.put(cart.getItems().get(0).getArticleEntity().getId(), Arrays.asList(amountDiscount));
		
		BigDecimal result = cart.calculateTotalItems(discountsByArticle);
		assertThat(result, equalTo(new BigDecimal(300d).setScale(2)));
	}
}
